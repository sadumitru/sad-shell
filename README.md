# SAD Shell

Created by Sorina-Aurelia Dumitru.

## Requirements

Install GCC and lib readline from package manager:

```bash
$ sudo apt install build-essential libreadline-dev
```

Install CMake version 3.17 or higher (download link [here](https://cmake.org/download/)).

## Build steps

Create a directory build, for example, within your projects directory and run CMake from there:

```bash
mkdir build
cd build
cmake ..
make
```

## Run the app

The binary file can be found at path `build/src/sad-shell`, path relative to the project root.

## Cleanup

Remove the `build` directory for cleanup because all the generated files are there.
