#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <string.h>
#include <strings.h>
#include <sys/wait.h>
#include <cd.h>
#include <help.h>
#include <version.h>
#include <fcntl.h>
#include "common.h"
#include "cp.h"
#include "mv.h"
#include "dirname.h"

typedef struct command {
    long argc;
    char* redirecting_file;
    char **argv;
} Command;

long count_quotes(char *string, char q) {
    // used to count the number or " or '
    long counter = 0;
    char *ptr = strchr(string, q);
    while (ptr != NULL) {
        counter++;
        ptr = strchr(ptr + 1, q);
    }

    return counter;
}

char *skip_quotes(char *string, char q) {
    //if there is a text between more pairs of any type of quotes, it keeps only the first pair
    long totalNoOfDq = count_quotes(string, q);
    char *tmp = malloc_or_die(sizeof(char) * (strlen(string) - totalNoOfDq - 2) + 1);

    int i = 0;
    int j = 0;

    while (string[i] != '\0') {
        while (string[i] == q && string[i] != '\0') {
            i++;
        }

        tmp[j] = string[i];
        j++;
        i++;
    }

    return tmp;
}

long args_counter(char *string) { // counts how many words are in the input.
    long counter = 0;
    unsigned long size = strlen(string); // the size of the input string
    /*
     * previousIsChar = -1 - means it is the end of the string;
     *               = 0 - means the last character before the current one was a " ";
     *               = 1 - means the character before the current one was not a " ";
     */
    short previousIsChar = -1;

    unsigned long i = 0; // iterator
    while (i < size) {  // iterate through the whole input string
        if (string[i] == '"' || string[i] == '\'') {
            char a = string[i];
            if (count_quotes(string, a) % 2) {
                return -1;
            }

            counter++;
            i++;
            while (string[i] != a && i < size) {
                i++;
            }
            i++;
            previousIsChar = 1;
//            char* tmp = malloc(sizeof (char ) * (i - j) + 1);
//            strncpy(tmp, string + j, i);
        } else {
            while (isspace(string[i])) { //go to the last " " in a sequence of " ".
                i++;
                previousIsChar = 0;  //say the
            }

            if (((previousIsChar == 0 && i < size) || previousIsChar == -1) && string[i] != '"' && string[i] != '\'') {
                //increment the counter only when there is a character that is not a space and the previous
                //character was a space;
                counter++;
                previousIsChar = 1;
            }
        }
        if (string[i] != '"' && string[i] != '\'' && string[i] != ' ') {
            i++;
        }
    }

    return counter;
}

char *get_commands_piped(char *input, long *pipeLocations, long pipeNr, long noOfPipes) {
    // takes the input and parses it so that it separates by pipe a command
    char *command;
    unsigned long characters;

    if (pipeNr == -1) {
        // before the first pipe
        command = calloc(pipeLocations[0] + 1, sizeof(char));
        validate_allocation_or_exit(command);

        strncpy(command, input, pipeLocations[0] - 1);
    } else if (pipeNr == noOfPipes - 1) {
        // after the last pipe
        long pipePosition = (noOfPipes == 1) ? 0 : pipeNr - 1;
        characters = strlen(input) - 1 - pipeLocations[pipePosition];
        command = malloc_or_die(sizeof(char) * characters + 1);
        strncpy(command, input + pipeLocations[pipeNr] + 1, characters);
    } else {
        // between executeCommands
        characters = pipeLocations[pipeNr + 1] - pipeLocations[pipeNr] - 1;
        command = malloc_or_die(sizeof(char) * characters + 1);
        strncpy(command, input + pipeLocations[pipeNr] + 1, characters);
    }

    return command;
}

char* space_redirection (char* cmd) {
    //looks for > in a char
    //if there is any, it puts it between spaces so that it will be easier to get the file path and separate it from
    //the rest of the command
    char* spaced_cmd;
    short space_left = 1;
    short space_right = 1;
    unsigned long size = strlen(cmd);
    char* redirecting_ptr = strchr(cmd, '>');
    if(redirecting_ptr == NULL) {
        return strdup(cmd);
    } else {
        if(cmd[redirecting_ptr - cmd - 1] != ' ') {
            size++;
            space_left = 0;
        }

        if (redirecting_ptr[1] != ' ') {
            size++;
            space_right = 0;
        }

        if(size == strlen(cmd)) {
            return strdup(cmd);
        }

        spaced_cmd = malloc_or_die(sizeof(char) * size + 1);
        strncpy(spaced_cmd, cmd, redirecting_ptr - cmd);
        if(space_left == 0) {
            strcat(spaced_cmd, " ");
        }

        if(space_right == 1) {
            strcat(spaced_cmd, redirecting_ptr);
        } else {
            strcat(spaced_cmd, "> ");
            strcat(spaced_cmd, redirecting_ptr + 1);
        }

        spaced_cmd[size] = '\0';
    }
    return spaced_cmd;
}

struct command *get_command(char *cmd) {
    // split the input string so that it returns an array of chars representing the array of all the word in the Command.
    long argCounter = args_counter(cmd); // the number of words in the input. Used for allocating memory for the array
    char **argv = malloc_or_die(sizeof(char *) * argCounter + 1); // the final array of words.

    int j = 0;
    char* tmp = space_redirection(cmd);

    char *ptr = strtok(tmp, " ");

    short met_redirection = 0;
    short waitForQuote = 0;
    char quote;
    while (ptr && ptr[0] != '>') {
        argv[j] = malloc_or_die(sizeof(char) * strlen(ptr) + 1);
        if (waitForQuote == 0) {
            if (strchr(ptr, '"') != 0) {
                waitForQuote = 1;
                quote = '"';
            } else if (strchr(ptr, '\'') != 0) {
                waitForQuote = 1;
                quote = '\'';
            }
            strcpy(argv[j], ptr);
            j++;
        } else {

            argv[j - 1] = realloc(argv[j - 1], sizeof(char) * (strlen(argv[j - 1] + strlen(ptr)) + 1) + 1);
            strcat(argv[j - 1], " ");
            strcat(argv[j - 1], ptr);
            strcat(argv[j - 1], "\0");
            if (ptr[strlen(ptr) - 1] == quote) {
                waitForQuote = 0;
                strcpy(argv[j - 1], skip_quotes(argv[j - 1], quote));
            }
        }
        ptr = strtok(NULL, " ");
        if(ptr != NULL && ptr[0] == '>') {
            met_redirection = 1;
        }
    }
    Command *command = malloc_or_die(sizeof(Command) + 1);

    if (met_redirection) {
        ptr = strtok(NULL, " ");
        command->redirecting_file = strdup(ptr);
    } else {
        command->redirecting_file = NULL;
    }

    argv[j] = (char *) 0;

    command->argc = argCounter;
    command->argv = argv;

//    free(tmp);
    return command; //each word in the initial command is a string in this array of strings
                    //except from > and the file path if there are any
}

long count_pipes(char *const input) {
    // counts the number of executeCommands in the input array

    int i = 0;
    char *pch = strchr(input, '|');
    while (pch != NULL) {
        i++;
        pch = strchr(pch + 1, '|');
    }

    return i;
}

long *get_pipe_positions(char *const input, long const noOfPipes) {
    long *pipePositions = malloc_or_die(sizeof(long) * noOfPipes + 1);

    long i = 0;
    char *pch = strchr(input, '|');
    while (pch != NULL) {
        pipePositions[i] = pch - input;
        pch = strchr(pch + 1, '|');
        i++;
    }

    // returns an arrays of long values representing the positions on which the executeCommands were found.
    return pipePositions;
}

Command **build_command_array(char *const input) {
    // creates the array of commands between the executeCommands
    long noOfPipes = count_pipes(input);
    Command **commands;

    if (noOfPipes == 0) {
        commands = malloc_or_die(sizeof(Command *) + 1);
        commands[0] = get_command(input);
        commands[1] = (Command *) 0;

        return commands;
    }

    long *pipeLocations = get_pipe_positions(input, noOfPipes);
    commands = malloc_or_die(sizeof(Command **) * (noOfPipes + 1) + 1);

    for (long i = -1; i < noOfPipes; i++) {
        char *command = get_commands_piped(input, pipeLocations, i, noOfPipes);
        commands[i + 1] = get_command(command);
        free(command);
    }

    commands[noOfPipes + 1] = (Command *) 0; // null is added at the end of the array so we know when to stop
    free(pipeLocations);

    return commands;
}


int command_handler(Command *command) {
    //checks if the command is implemented by me and if it is it executes it, otherwise returns 0
    char* internal_commands[7] = {"help", "cd", "dirname", "cp", "mv", "exit", "version"};

    int command_no = 0;
    char* command_name = strdup(command->argv[0]);
    for (int i = 0; i < 7; i++) {
        if (strcmp(command_name, internal_commands[i]) == 0) {
            command_no = i + 1;
            break;
        }
    }

    switch (command_no) {
        case 1:
            my_help();
            return 1;
        case 2:
            my_cd((int)command->argc, command->argv);
            return 1;
        case 3:
            dirname((int)command->argc, command->argv);
            return 1;
        case 4:
            my_cp((int)command->argc, command->argv);
            return 1;
        case 5:
            mv((int)command->argc, command->argv);
            return 1;
        case 6:
            exit(EXIT_SUCCESS);
        case 7:
            version();
            return 1;
        default:
            break;
    }

    return 0;
}

void execute_commands(Command **commands) {
    //save current in file descriptors
    int savedInFD = dup(0);
    //save current out file descriptors
    int savedOutFD = dup(1);
    int inFD = dup(savedInFD); //use default input
    int outFD;

    int i = 0;
    while (commands[i] != NULL) { //iterate through the commands
        dup2(inFD, 0); //redirect input
        close(inFD);

        if (commands[i + 1] == NULL) { //if there is only one Command or it is the last Command
            if(commands[i]->redirecting_file != NULL) { //if the output must be redirected;
                outFD = open(commands[i]->redirecting_file, O_WRONLY | O_CREAT | O_TRUNC,
                             S_IRUSR | S_IWUSR); //redirect input to the file
            } else { //else use default output
            outFD = dup(savedOutFD);
            }
        } else {
            // there are piped commands
            int fd[2];  // pipe file descriptors
            int pipeReturn = pipe(fd); // create pipe
            if (pipeReturn < 0) { // pipe created unsuccessfully
                perror("pipe");
            }

            int outFD2;
            if(commands[i]->redirecting_file != NULL) { //if the current command has redirection
                outFD2 = openat(fd[1], commands[i]->redirecting_file, O_WRONLY | O_CREAT | O_TRUNC,
                             S_IRUSR | S_IWUSR); // redirect input to pipe and file
                dup2(outFD2, 1);
                close(outFD2); //close file
            }

            //makes input for the next command to come from the pipe
            inFD = fd[0];
            outFD = fd[1]; //makes output to go to the pipe
        }

        dup2(outFD, 1); //redirect output
        close(outFD);

        if (command_handler(commands[i])) { //if the command is one implemented by me,
            i++;
            continue;
        }

        int pid = fork(); // create child process
        if (pid == 0) { // child process
            // child
            int execReturn = execvp(commands[i]->argv[0], commands[i]->argv); // execute the current Command
            if (execReturn < 0) { //exec unsuccessful
                perror("exec");
            }
            exit(0);
        }

        if (pid < 0) { // fork unsuccessfully created
            perror("fork");
        }

        if (pid > 0) {
            // parent
            if (wait(0) == -1) {  // the parent process waits for the child process.
                perror("wait");
            }
        }

        i++; // go to the next Command
    }

    // redirecting reading to stdin and writing to stdout
    dup2(savedOutFD, 1);
    dup2(savedInFD, 0);
    close(outFD);
    close(savedInFD);
}

int only_spaces(char *input) {
    //checks if the Command contains only " "
    //it is used to know if we add the Command to history or we just skip it.
    unsigned long i = 0;
    unsigned long size = strlen(input);
    while (i < size) {
        if (!isspace(input[i])) {
            return 0;
        }
        i++;
    }

    return 1;
}

int null_input(char *input) {
    //checks if the input is an empty string
    //used when checking if we should add the Command to history or not
    if (strcmp(input, "") == 0) {
        return 1;
    }

    return 0;
}

void free_commands(Command **commands) {
    //checks if there are any characters in the command.
    //it is used to avoid adding blank spaces in history.
    int i = 0;
    while (commands[i] != NULL) {
        for (int j = 0; j < commands[i]->argc; j++) {
            free(commands[i]->argv[j]);
        }
        i++;
    }
}

void start_shell() {
    //the shell itself
    //calls function to parse the initial command and then to execute it
    //until it receives an exit, it will wait to be prompted by the user
    chdir(getenv("HOME"));

    char *input;
    char ps[4];

    sprintf(ps, "$> ");
    while ((input = readline(ps)) != NULL) { //waits for input from the user
        if (null_input(input) || only_spaces(input)) { //checks if the input is valid
            continue; //if it is not, we just skip it
        }

        // executes the external commands, including the one with executeCommands
        Command **commands = build_command_array(input);

        execute_commands(commands);
        free_commands(commands);

        add_history(input);
        free(input);
        sprintf(ps, "$> ");
    }
}

int main(int argc, char *argv[]) {
    start_shell();

    exit(EXIT_SUCCESS);
}
