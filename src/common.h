#ifndef SAD_SHELL_COMMON_H
#define SAD_SHELL_COMMON_H

void *malloc_or_die(size_t size);
int file_exists(const char *path);
int is_dir(const char *path);
short read_option();
char *strcat_paths(const char *path, const char *file);
void validate_allocation_or_exit(const char *str);
int check_file_permission(const char* path, int type);
int is_regular_file(char *path);
char *basename(char *path);

#endif
