#include <malloc.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <argz.h>
#include <unistd.h>

//common functions for implemented commands

int is_regular_file(char *path) {
    //checks is a file is a regular file
    struct stat st;

    if (stat(path, &st) == -1 || S_ISDIR(st.st_mode)) {
        return 0;
    }

    return S_ISREG(st.st_mode);
}

char *basename(char *path) { //get basename of a file
    char *ptr = strrchr(path, '/');

    return ptr == NULL ? path : ptr + 1;
}

void *malloc_or_die(size_t size) { //allocates memory and checks if the pointer was successfully created and if it is
                                // different from NULL
    void *pVoid = malloc(size);

    if (!pVoid && (size > 0)) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }

    return pVoid;
}

void validate_allocation_or_exit(const char *str) {  //used to validate calloc
    if (str == NULL) {
        exit(EXIT_FAILURE);
    }
}

int file_exists(const char *path) {  //checks if a file exist
    struct stat st;
    return (stat(path, &st) == 0);
}

int is_dir(const char *path) {
    //checks if a file is a dir
    struct stat st;

    if (stat(path, &st) == -1) {
        return 0;
    }
    return S_ISDIR(st.st_mode);
}

short read_option() {
    //used if there is -i and asks user for an answer (yer or no)
    char option[256];
    fgets(option, sizeof(option), stdin);
    char *ptr = strchr(option, '\n');
    if (ptr != NULL) {
        *ptr = '\0';
    }

    if (strcasecmp(option, "y") == 0 || strcasecmp(option, "yes") == 0) {
        return 1;
    }

    return 0;
}

char *strcat_paths(const char *path, const char *file) {
    //concatenate paths, putting / between them
    unsigned long final_path_size = strlen(path) + strlen(file) + 1;
    char *final_path = malloc_or_die((sizeof(char) * final_path_size + 1) + 1);

    strcpy(final_path, path);
    strcat(final_path, "/");
    strcat(final_path, file);
    final_path[final_path_size] = '\0';

    return final_path;
}

int check_file_permission(const char* path, int type) {
    //checks the user has certain permissions for a file
    short result = 1;
    int permission_value = access(path, type);
    if (permission_value != 0) {
        if (errno == EACCES) {
            result = 0; //access denied
        }
    }

    return result;
}