#include <stdio.h>
#include "version.h"

void version() { //gives info about shell version and name, author and copyright
    printf("%s", "sad_shell v0.0.1\n");
    printf("%s", "Copyright (C) Sorina-Aurelia Dumitru\n");
    printf("%s", "Written by Sorina-Aurelia Dumitru @ West University of Timisoara\n");
}