#ifndef SAD_SHELL_DIRNAME_H
#define SAD_SHELL_DIRNAME_H

char *get_tilda_path(char *path);
int dirname(int argc, char **argv);

#endif
