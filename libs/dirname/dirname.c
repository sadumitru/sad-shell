#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../../src/common.h"


char *get_tilda_path(char *path) { //used when there is ~ in the path
    char *absPath = NULL;
    const char *homePath = getenv("HOME");  //get home path

    if (strlen(path) > 0) {
        if (strlen(path) == 1) { //if there is only ~, the path is the home path
            if (path[0] == '~') {
                absPath = malloc_or_die(sizeof(char) * strlen(homePath) + 1);
                strcpy(absPath, homePath);
            }
        } else if (strlen(path) > 1) {  // otherwise concatenate the home path with the rest of the path
            if (path[0] == '~' && path[1] == '/') {
                absPath = malloc_or_die(sizeof(char) * (strlen(homePath) + strlen(path) + 1));
                strcpy(absPath, homePath);
                strcat(absPath, path + 1);
                absPath[strlen(absPath) - 1] = '\0';
            } else {
                absPath = strdup(path);
            }
        } else {
            absPath = strdup(path);
        }
    }

    return absPath;
}

int dirname(int argc, char **argv) { //strip last component from file name
    if (argc == 1) { //id there is no argument to dirname
        fprintf(stderr, "%s argument required", argv[0]);
        return 1;
    }

    for (int i = 1; i < argc; i++) {
        char *path = strchr(argv[i], '~') != NULL ? get_tilda_path(argv[i]) : strdup(argv[i]); //if there is tilda, transform path to absolut path
        validate_allocation_or_exit(path);

        char *ptr = strrchr(path, '/'); // get the last position of the last file name in the path

        if (ptr == NULL) { // if there is no / get the dirname of the current dir
            fprintf(stdout, "%s\n", ".");
        } else {
            if (ptr - path + 1 == strlen(path)) {
                path[strlen(path) - 1] = '\0';
            }

            ptr = strrchr(path, '/'); // get dirname
            path[ptr - path] = '\0';

            fprintf(stdout, "%s\n", path);
        }

        if (i < argc) {
            fprintf(stdout, "\n");
        }

        free(path);
    }
    return 0;
}
