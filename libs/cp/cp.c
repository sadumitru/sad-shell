#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include "../../src/common.h"

typedef struct cp_option { //flags for cp
    short interactive;
    short recursive;
    short target;
    short verbose;
} CpOption;

void copy_content(int src_fd, int dst_fd) { //function used to copy content from a file to another
    char *buffer = malloc_or_die(sizeof(char) * 1024); //the buffer used for reading
    int b_written; // how much it wrote
    int b_read; //how much it read
    while ((b_read = read(src_fd, buffer, 1024)) > 0) { //as long as there are bytes to be read
        if (b_read < 0) { //check if reading was successful
            perror("Error reading data from source file");
        }

        buffer[b_read] = '\0'; //add end to buffer

        b_written = write(dst_fd, buffer, b_read); //write to destination file using its fd
        if (b_written != b_read) { //check if the number of bytes read is equal to the number of bytes written
            perror("Error writing to destination file");
            return;
        }
    }

    free(buffer);
}

void copy_file(const char *src_path, const char *dst_path,
               const CpOption options) { //write and open/create dest file using the function above
    int source_file_descriptor;
    int src_permissions = check_file_permission(src_path, R_OK);
    //check if it has permission to read from src file
    if (src_permissions) {
        source_file_descriptor = open(src_path, O_RDONLY);  //try to open the file in read mode
        if (source_file_descriptor < 0) {
            perror("Error opening source file");
            return;
        }
    } else { //error if not
        fprintf(stderr, "Not enough permissions to read from %s", src_path);
        return;
    }

    char *target = strdup(dst_path);

    if (options.interactive) { //if i is passed as an option, ask user if he wants to overwrite the existing file
        if (file_exists(target)) {
            fprintf(stdout, "cp: overwrite '%s'? ", target);
            if (!read_option()) {
                return;
            }
        }
    }

    int destination_file_descriptor;
    //checks if it has write permission for dst file
    int dst_permissions = check_file_permission(dst_path, W_OK);
    if (dst_permissions) { //if we have, open file or create if it doesn't exist or truncate it to length 0
        destination_file_descriptor = open(target, O_WRONLY | O_CREAT | O_TRUNC,
                                           S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH |
                                           S_IWOTH); //check read write permissions
        //for owner, group owner and other users
        if (destination_file_descriptor < 0) { //check if opened successfully
            perror("Error opening destination file");
            return;
        }
    } else {
        fprintf(stderr, "Not enough permissions to write to %s", dst_path);
        return;
    }

    copy_content(source_file_descriptor, destination_file_descriptor); //copy the content from one file to the other

    //close the files and check if it was done successfully
    if (close(source_file_descriptor) == -1) {
        perror("Error closing source file");
    }

    if (close(destination_file_descriptor) == -1) {
        perror("Error closing destination file");
    }

    //-v - show what is being moved
    if (options.verbose) {
        fprintf(stdout, "'%s' -> '%s'\n", src_path, target);
    }

    free(target);
}

void copy_recursive(const char *src_path, const char *dst_path, const CpOption options) {
    //used to copy directories
    struct dirent *file;
    DIR *dir = opendir(src_path);
    struct stat st;

    if (!dir) { //if it is not a directory, do a simple copy
        copy_file(src_path, dst_path, options);
        return;
    }

    mkdir(dst_path, 0755); //create dir to the dest file, with r w x permissions for owner
    // group owner and other users - only read and execute permissions

    while ((file = readdir(dir))) { //as long as the file is a directory
        if (strcmp(file->d_name, ".") != 0 && strcmp(file->d_name, "..") !=
                                              0) {  //if it is different than current dir and parent dir, so it won't loop infinitely
            char *src_new_path = strcat_paths(src_path, file->d_name); //create the new path, concatenating to the
            //src path the name of the file to be copied

            stat(src_new_path, &st);
            char *dst_new_path = strcat_paths(dst_path, file->d_name); //go deeper into the file

            if (S_ISDIR(st.st_mode)) { //if it is a directory too
                mkdir(dst_new_path, 0755); //create it in mode 0755
                copy_recursive(src_new_path, dst_new_path, options); //get in the function again
            } else {
                copy_file(src_new_path, dst_new_path, options); //if it is a file just copy it
            }

            //free the new paths
            free(dst_new_path);
            free(src_new_path);
        }
    }

    closedir(dir);
}

int my_cp(int argc, char **argv) { //the actual cp
    //initialize the options flags with 0
    CpOption options = {0, 0, 0, 0};
    //set opterr and optind equal to 0 to avoid odd behaviour
    opterr = 0;
    optind = 0;
    char *target = NULL;
    int c;

    while ((c = getopt(argc, argv, "irRvt:")) != -1) { // as long as there are options
        //initialize its flag with one
        switch (c) {
            case 'i':
                options.interactive = 1;
                break;
            case 'r':
            case 'R':
                options.recursive = 1;
                break;
            case 'v':
                options.verbose = 1;
                break;
            case 't':
                options.target = 1;
                target = strdup(optarg); //copy the target directory
                break;
            case '?': //unexpected characters passed as options
                if (optopt == 't')
                    fprintf(stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf(stderr,
                            "Unknown option character `\\x%x'.\n",
                            optopt);
                return 1;
            default:
                abort();
        }
    }

    int src_end = argc; //the number of source files
    int non_opt_args = argc - optind; //the number of non optional arguments
    //if there is -t, the dst file must be a directory
    if (options.target) {
        if (!is_dir(target)) {
            fprintf(stderr, "cp: failed to access '%s': No such file or directory\n", target);
            return 1;
        }
    } else {
        target = strdup(argv[argc - 1]); //get the target file

        if (non_opt_args >= 2) {
            if (!is_dir(target) &&
                non_opt_args != 2) { //if there are more than two non-opt args, the target must be a directory
                fprintf(stderr, "cp: target '%s' is not a directory\n", target);
                return 1;
            }
            src_end = argc - 1; //the number of source files (exclude the dest file)
        }
    }

    //optind gives the position of the first non opt argument
    for (int index = optind; index < src_end; index++) { //iterate through the source files
        if (!options.recursive && ((non_opt_args >= 1 && options.target) || (non_opt_args > 1 && !options.target))) {
            if (is_dir(argv[index])) {
                fprintf(stderr, "cp: -r not specified; omitting directory '%s'\n", argv[index]);
                continue;
            }
        }

        //get the current source file and the destination file
        char *dest = strdup(target);
        char *src = argv[index];

        if (non_opt_args >= 1) {
            char *file_name = strrchr(argv[index], '/'); //get the name of the file

            if (file_name != NULL) { //if there is no / just add one to the strtchr pointer
                src = file_name + 1;
            }

            if (is_dir(target)) { //if the destination is a target
                dest = strcat_paths(target, src); //concatenate the name of the file to the dst path so that
                                                    //it can be written
            }
        }

        if (options.recursive) { //is there is -r call the recursive function
            copy_recursive(argv[index], dest, options);
        } else {
            copy_file(argv[index], dest, options);
        }
    }

    free(target);
    return 0;
}
