#ifndef SAD_SHELL_CP_H
#define SAD_SHELL_CP_H

typedef struct CpOption CpOption;

void copy_content(int src_fd, int dst_fd);
void copy_files(const char *src_path, const char *dst_path, CpOption options);
void copy_recursive(const char *src_path, const char *dst_path, CpOption options);
int my_cp(int argc, char **argv);

#endif
