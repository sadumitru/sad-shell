#ifndef SAD_SHELL_MV_H
#define SAD_SHELL_MV_H

typedef struct MvOption MvOption;

void move_content(int src_fd, int dst_fd);
int remove_files(const char  *path);
short move_file(const char *src_path, const char *dst_path, MvOption options, short remove_src_file);
int my_mv(int argc, char **argv);
int mv(int argc, char **argv);
void initialize_options(MvOption *options);
short is_interactive(const char *target, MvOption *options);
void mv_directory(const char * src_path, const char * dst_path, MvOption options);
char *get_new_target(const char *src_path, const char *dst_path);
void create_backup(const char *dest, MvOption *options);
int count_files_in_dir(char *path);
char *get_absolute_current_path();
#endif
