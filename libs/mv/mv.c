#include <malloc.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <argz.h>
#include <getopt.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include "../../src/common.h"

typedef struct mv_option { //options for mv
    short interactive;
    short target;
    short suffix;
    char *suffix_str;
    short backup;
} MvOption;

void initialize_options(MvOption *options) {
    options->interactive = 0;
    options->target = 0;
    options->backup = 0;
    options->suffix = 0;
}


short is_interactive(const char *target, MvOption *options) { //asks user for y or n if -i
    if (!options->interactive) {
        return 0;
    }
    short user_option = 0;
    if (file_exists(target)) {
        fprintf(stdout, "mv: overwrite '%s'? ", target);
        user_option = read_option();
    } else {
        return 1;
    }
    return user_option;
}


short move_content(int src_fd, int dst_fd) { //function to move content from a file to another file
    short move_success = 1;
    char *buffer = malloc_or_die(sizeof(char) * 1024);
    int b_written;
    int b_read;
    while ((b_read = read(src_fd, buffer, 1024)) > 0) {
        if (b_read < 0) {
            perror("Error reading data from source file");
            move_success = 0;
        }

        buffer[b_read] = '\0';

        b_written = write(dst_fd, buffer, b_read);
        if (b_written != b_read) {
            perror("Error writing to destination file");
            return 1;
        }
    }

    free(buffer);
    return move_success;
}

int remove_files(const char *path) {
    //function remove files
    if (remove(path) != 0) {
        perror("mv file deletion unsuccessful");
        return 0;
    }

    return 1;
}

//remove_src_file is used to know if the src file should be deleted or not
//because the function is used to backup files too so in that case there is no need delete the src file
short move_file(const char *src_path, const char *dst_path, MvOption *options, short remove_src_file) {
    //check if it has permission to read from src file
    int source_file_descriptor;
    int src_permissions = check_file_permission(src_path, R_OK);
    if (src_permissions) {
        source_file_descriptor = open(src_path, O_RDONLY); //if it has, it opens it
        if (source_file_descriptor < 0) {
            perror("Error opening source file");
            return 1;
        }
    } else {
        fprintf(stderr, "Not enough permissions to read from %s", src_path);
        return 1;
    }

    short user_option = 1;
    char *target = strdup(dst_path);
    if (options->interactive == 1) {
        user_option = is_interactive(target, options);
        if (user_option == 0) {
            return 0;
        }
    }

    int destination_file_descriptor;
    //checks if it has write permission for dst file
    int dst_permissions = check_file_permission(dst_path, W_OK);
    if (dst_permissions) {
        destination_file_descriptor = open(target, O_WRONLY | O_CREAT | O_TRUNC, //open dest file
                                           S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
        if (destination_file_descriptor < 0) {
            perror("Error opening destination file");
            return 1;
        }
    } else {
        fprintf(stderr, "Not enough permissions to write to %s", dst_path);
        return 1;
    }

    short successful_move = move_content(source_file_descriptor, destination_file_descriptor);
    if (successful_move && remove_src_file) { //if the move was successful, it removes the source file
        remove_files(src_path);
    }

    //close the files and chef if it was done successfully
    if (close(source_file_descriptor) == -1) {
        perror("Error closing source file");
    }

    if (close(destination_file_descriptor) == -1) {
        perror("Error closing destination file");
    }

    free(target);
    return user_option;
}

char *get_new_target(const char *src_path, const char *dst_path) {
    char *src_name = strrchr(src_path, '/');

    if (src_name == NULL) {
        return strcat_paths(dst_path, src_path);
    }
    return strcat_paths(dst_path, src_name + 1);
}


void mv_directory(const char *src_path, const char *dst_path, MvOption *options, short remove_dir) {
    //recursively moves a directory
    struct dirent *file;
    DIR *dir = opendir(src_path);
    struct stat st;

    if (!dir) { //if the file is not a directory, it simply moves it
        move_file(src_path, dst_path, options, 1);
        return;
    }

    char *src_name = strrchr(src_path, '/');

    if (strcmp(dst_path, ".") == 0) {
        if (src_name) {
            dst_path = strdup(src_name + 1);
        } else {
            dst_path = strdup(src_path);
        }
    }

    char *target = strdup(dst_path);
    if (!file_exists(dst_path)) {
        mkdir(dst_path, 0755); //create dir to the dest file, with r w x permissions for owner
        // group owner and other users - only read and execute permissions
    } else {
        char *tmp = strdup(src_path);
        if (src_name) {
            free(tmp);
            tmp = strcat_paths(src_path, src_name + 1);
        }

        target = get_new_target(src_path, dst_path);
        if (!file_exists(tmp) || !is_dir(tmp)) {
            free(target);
            target = strdup(dst_path);
        }

        if (!file_exists(target)) {
            mkdir(target, 0755);
        } else {
            if (options->interactive == 1) {
                short overwrite = is_interactive(target, options); //if file exists and there is -i ask for user answer
                options->interactive = 0;
                if (overwrite == 0) {
                    return;
                }
            }
        }
    }

    while ((file = readdir(dir))) {
        //checks if src file is different from . and .. because otherwise the while would loop infinitely
        if (strcmp(file->d_name, ".") != 0 && strcmp(file->d_name, "..") != 0) {
            char *src_new_path = strcat_paths(src_path, file->d_name);

            stat(src_new_path, &st);
            char *dst_new_path = strcat_paths(target, file->d_name); //go deeper into the file

            if (S_ISDIR(st.st_mode)) { //if it is a directory too
                char *tmp = strcat_paths(src_path, file->d_name);
                if (!file_exists(tmp) || !is_dir(tmp)) {
                    free(dst_new_path);
                    dst_new_path = strdup(target);
                }
                free(tmp);
                mkdir(dst_new_path, 0755); //create it in mode 0755
                mv_directory(src_new_path, dst_new_path, options, 1); //get in the function again
            } else {
                move_file(src_new_path, dst_new_path, options, 1); //if it is a file just copy it
            }

            //free the new paths
            free(dst_new_path);
            free(src_new_path);
        }
    }

    free(target);
    //remove the initial directory
    if (remove_dir == 1) {
        remove_files(src_path);
    }
    closedir(dir);
}


void create_backup(const char *dest, MvOption *options) { //creates a backup of a file if there is -b
    if (options->backup && file_exists(dest)) {
        unsigned long backup_dest_size = strlen(dest) + strlen(options->suffix_str);
        char *backup_dest = malloc_or_die(sizeof(char) * backup_dest_size + 1);
        strcpy(backup_dest, dest);
        strcat(backup_dest, options->suffix_str);
        backup_dest[backup_dest_size] = '\0';
        if (!is_dir(dest)) {
            move_file(dest, backup_dest, options, 0);
        } else if (!file_exists(backup_dest)) {
            mv_directory(dest, backup_dest, options, 0);
        }
    }
}


int count_files_in_dir(char *path) {
    //counts the number of files in a directory
    struct dirent *file;
    DIR *dir = opendir(path);
    int counter = 0;

    while ((file = readdir(dir))) {
        //checks if src file is different from . and .. because otherwise the while would loop infinitely
        if (strcmp(file->d_name, ".") != 0 && strcmp(file->d_name, "..") != 0) {
            counter++;
        }
    }

    return counter;
}


char *get_absolute_current_path() {
    // used to get the absolute path instead of the relative one
    long size = pathconf(".", _PC_PATH_MAX);
    char *buf = malloc_or_die(sizeof((size_t) size));

    return getcwd(buf, (size_t) size);
}


int mv(int argc, char **argv) {
    int result_code = 1;
    if (argc < 3) {
        perror("mv: Not enough arguments");
        return 1;
    }
    //initialize the options flags with 0
    MvOption *options = malloc_or_die(sizeof(MvOption) + 1);
    initialize_options(options);

    //set opterr and optind equal to 0 to avoid odd behaviour
    opterr = 0;
    optind = 0;
    char *target = NULL;
    options->suffix_str = malloc_or_die(sizeof(char) + 1);
    strcpy(options->suffix_str, "~"); //initialize the backup suffix with ~
    int c;

    while ((c = getopt(argc, argv, "iS:bt:")) != -1) { // as long as there are options
        //initialize its flag with one
        switch (c) {
            case 'i':
                options->interactive = 1;
                break;
            case 'S':
                options->suffix = 1;
                //save the new suffix gave as parameter
                if (strcmp(optarg, "") != 0) {
                    free(options->suffix_str);
                    options->suffix_str = strdup(optarg);
                }
                break;
            case 'b':
                options->backup = 1;
                break;
            case 't':
                options->target = 1;
                target = strdup(optarg); //copy the target directory
                break;
            case '?':
                //unexpected characters passed as options
                if (optopt == 't')
                    fprintf(stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf(stderr,
                            "Unknown option character `\\x%x'.\n",
                            optopt);
                return 1;
            default:
                abort();
        }
    }

    int src_end = argc; //the number of source files
    int non_opt_args = argc - optind; //the number of non optional arguments

    if (options->target) {  //if there is -t, the dst file must be a directory
        if (!file_exists(target)) {
            fprintf(stderr, "mv: failed to access '%s': No such file or directory\n", target);
            return 1;
        }
        if (!is_dir(target)) {
            fprintf(stderr, "mv: target '%s' is not a directory\n", target);
            return 1;
        }
    } else {
        target = strdup(argv[argc - 1]); //get the target file

        if (non_opt_args >= 2) {
            if (!is_dir(target) &&
                non_opt_args != 2) {//if there are more than two non-opt args, the target must be a directory
                fprintf(stderr, "mv: target '%s' is not a directory\n", target);
                return 1;
            }
            src_end = argc - 1; //the number of source files (exclude the dest file)
        }
    }


    for (int index = optind; index < src_end; index++) {
        int arg_return_status = 1;
        if (!file_exists(argv[index])) {
            fprintf(stderr, "mv: failed to access '%s': No such file or directory\n", argv[index]);
            arg_return_status = 0;
            continue;
        }

        //if src is a file
        if (is_regular_file(argv[index])) {
            if (file_exists(target) && is_regular_file(target)) { //if the target exists and it is a file too
                if (is_interactive(target, options) == 1) { //check if the user wants to overwrite
                    if (options->backup) {  //check if it wants a backup file of the target
                        create_backup(target, options); //create the backup file
                    }

                    arg_return_status = rename(argv[index], target);  //overwrite the target file
                }
            }
        }
        if (!file_exists(target)) {  //if the target file doesn't exist, just rename the source file
            arg_return_status = rename(argv[index], target);
        }

        if (is_dir(target)) { //if the target is a directory
            char *base_name = basename(argv[index]);
            char *dir_file_path = strcat_paths(target, base_name);

            if (file_exists(
                    dir_file_path)) { //if the directory already contains a file with the same name as the src file
                if (is_interactive(dir_file_path, options) == 1) { //ask the user if he wants to overwrite it
                    if (options->backup) { //create a backup if wwanter
                        create_backup(dir_file_path, options);
                    }

                    arg_return_status =  rename(argv[index], dir_file_path);
                }
            } else {
                arg_return_status = rename(argv[index],
                              dir_file_path); //if the directory doesn't contain a file with the same name, just move the src file in the dir
            }
        }

        if (is_dir(argv[index])) { //if the src is a directory
            if (file_exists(target)) { // if the target is a regular file, illegal operation
                if (is_regular_file(target)) {
                    perror("cd: Illegal operation: cannot move directory to file");
                    arg_return_status = 1;
                }
                if (is_dir(target)) { //if it is a dir
                    if (count_files_in_dir(argv[index]) != 0) { //if the src dir is not empty
                        char *base_name = basename(argv[index]);
                        char *new_dir_path = strcat_paths(target, base_name);
                        if (is_interactive(new_dir_path, options) ==
                            1) { //ask the user if he wants to overwrite the dir
                            if (options->backup) { //create backup is necessary
                                create_backup(new_dir_path, options);
                            }

                            arg_return_status = rename(argv[index], new_dir_path);
                        }
                    } else { //if the directory is empty, overwrite it
                        char *new_target;

                        if (strcmp(target, ".") == 0) { //if the relative path is given, use the absolute one
                            new_target = get_absolute_current_path();
                        } else {
                            new_target = strdup(target);
                        }

                        char *base_name = basename(argv[index]);
                        char *final_target = strcat_paths(new_target, base_name);

                        int rename_result = rename(argv[index], final_target); //overwrite the dir

                        free(final_target);
                        free(new_target);

                        arg_return_status = rename_result;
                    }

                }
            } else {
                arg_return_status = rename(argv[index], target); //if the target doesn't exist, just rename the src dir
            }
        }

        if(arg_return_status == 0) {
            result_code = 0;
        }
    }

    return result_code;
}


//used for mv implemented without rename
int my_mv(int argc, char **argv) {  //mv without rename
    if (argc < 3) {
        perror("mv: Not enough arguments");
        return 1;
    }
    //initialize the options flags with 0
    MvOption *options = malloc_or_die(sizeof(MvOption) + 1);
    initialize_options(options);

    //set opterr and optind equal to 0 to avoid odd behaviour
    opterr = 0;
    optind = 0;
    char *target = NULL;
    options->suffix_str = malloc_or_die(sizeof(char) + 1);
    strcpy(options->suffix_str, "~"); //initialize the backup suffix with ~
    int c;

    while ((c = getopt(argc, argv, "iS:bt:")) != -1) { // as long as there are options
        //initialize its flag with one
        switch (c) {
            case 'i':
                options->interactive = 1;
                break;
            case 'S':
                options->suffix = 1;
                //save the new suffix gave as parameter
                if (strcmp(optarg, "") != 0) {
                    free(options->suffix_str);
                    options->suffix_str = strdup(optarg);
                }
                break;
            case 'b':
                options->backup = 1;
                break;
            case 't':
                options->target = 1;
                target = strdup(optarg); //copy the target directory
                break;
            case '?':
                //unexpected characters passed as options
                if (optopt == 't')
                    fprintf(stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf(stderr,
                            "Unknown option character `\\x%x'.\n",
                            optopt);
                return 1;
            default:
                abort();
        }
    }

    int src_end = argc; //the number of source files
    int non_opt_args = argc - optind; //the number of non optional arguments

    if (options->target) {  //if there is -t, the dst file must be a directory
        if (!file_exists(target)) {
            fprintf(stderr, "mv: failed to access '%s': No such file or directory\n", target);
            return 1;
        }
        if (!is_dir(target)) {
            fprintf(stderr, "mv: target '%s' is not a directory\n", target);
            return 1;
        }
    } else {
        target = strdup(argv[argc - 1]); //get the target file

        if (non_opt_args >= 2) {
            if (!is_dir(target) &&
                non_opt_args != 2) {//if there are more than two non-opt args, the target must be a directory
                fprintf(stderr, "mv: target '%s' is not a directory\n", target);
                return 1;
            }
            src_end = argc - 1; //the number of source files (exclude the dest file)
        }
    }

    int was_interactive = options->interactive;
    //optind gives the position of the first non opt argument
    for (int index = optind; index < src_end; index++) {
        if (!file_exists(target) && is_dir(argv[index])) {
            mkdir(target, 0755);
            rename(argv[index], target);
            return 0;
        }

        if (is_dir(argv[index]) && !is_dir(target)) {
            perror("mv: cannot move directory into file\n");
            continue;
        }
        //get the current the destination file
        char *dest = strdup(target);

        if (non_opt_args >= 1) {
            if (options->target || non_opt_args > 2) { //if there are more than 2 non opt args or there is -t as option
                if (!file_exists(argv[index])) { //the target must be a dir
                    fprintf(stderr, "mv: cannot stat '%s': No such file or directory\n", argv[index]);
                    continue;
                }
                char *file_name = strrchr(argv[index], '/');
                if (file_name == NULL) { //change the dst path based on the format of the name of the file
                    dest = strcat_paths(target, argv[index]);
                } else {
                    dest = strcat_paths(target, file_name + 1);
                }
            }
        }

        //if everything was successful, create back up if it is asked for
        short successful_move = 1;
        if (is_dir(argv[index])) {
            char *new_dest = get_new_target(argv[index], dest);

            if (file_exists(new_dest)) {
                create_backup(new_dest, options);
            }

            mv_directory(argv[index], dest, options, 1);
        } else {
            successful_move = move_file(argv[index], dest, options, 1);
            if (successful_move) {
                create_backup(dest, options);
            }
        }

        if (was_interactive) {
            options->interactive = 1;
        }
    }


    // free memory
    free(options->suffix_str);
    free(target);
    return 0;
}
