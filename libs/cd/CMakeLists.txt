# Create a library called "Cp" which includes the source file "cp.c".
# The extension is already found. Any number of sources could be listed here.
add_library (Cd cd.c)

# Make sure the compiler can find include files for our Hello library
# when other libraries or executables link to Hello
target_include_directories (Cd PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})