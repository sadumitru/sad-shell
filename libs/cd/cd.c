#include <unistd.h>
#include <stdio.h>
#include "cd.h"
#include "../../src/common.h"

int my_cd(int argc, char** argv) { //implementation of cd using chdir
    //check if we have permissions for reading and executing the file.
    // without execute permission the cd will give an error
    int read_permission = check_file_permission(argv[1], R_OK);
    int execute_permission = check_file_permission(argv[1], X_OK);
    if(read_permission != 1 && execute_permission != 1) { //permission denied
        fprintf(stderr, "Cannot open directory. Permission denied.");
        return 1;
    } else { //enough permissions
        int chdir_return = chdir(argv[1]); //change dir to the path in argv[1]
        if (chdir_return != 0) { //check if chdir was successful or not
            perror("cd");
            return 1;
        }
    }

    return 0; // the chdir was successfull
}